#!/usr/bin/env python3

import requests
from urllib.parse import urlencode

API_URL = "https://gitlab.example.org/api/v4"
PRIVATE_TOKEN = "*********************"


def get_user_page(page, private_token):
    query_params = {'private_token': private_token,
            'page': page,
            'per_page': 100}
    return requests.get(API_URL + "/users?" + urlencode(query_params))

def get_users(private_token):
    first_page = get_user_page(1, private_token)
    userlist = first_page.json()
    total_pages = int(first_page.headers['X-Total-Pages'])
    
    for i in range(2,total_pages+1):
        userlist.extend(get_user_page(i, private_token).json())
    return userlist


userlist = get_users(PRIVATE_TOKEN)

for user in userlist:
    query_params = {'private_token': PRIVATE_TOKEN,
            'projects_limit': 1000}
    requests.put(API_URL + f"/users/{user['id']}?" + urlencode(query_params))
